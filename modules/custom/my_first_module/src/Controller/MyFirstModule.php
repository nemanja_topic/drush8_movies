<?php

namespace Drupal\my_first_module\Controller;

class HelloWorldController{
    public function hello(){
        return array(
            '#title' => 'Hello World'
            '#markup' => 'This is some content.'
        );
    }
}